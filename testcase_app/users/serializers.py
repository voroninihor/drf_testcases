"""Serializer for user objects."""
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.authtoken.models import Token


class UserRegistrationSerializer(serializers.ModelSerializer):
    """Serializer data for registration users."""

    password = serializers.CharField(write_only=True)
    confirm_password = serializers.CharField(write_only=True)

    class Meta:
        """Meta-data for user's registration serializer."""

        model = User
        fields = ("id", "username", "email",
                  "password", "confirm_password", "date_joined")

    def create(self, validated_data):
        """Create user."""
        del validated_data["confirm_password"]
        return super(UserRegistrationSerializer, self).create(validated_data)

    def validate(self, attrs):
        """Validate user's password."""
        if attrs.get('password') != attrs.get('confirm_password'):
            raise serializers.ValidationError("Those passwords don't match.")
        return attrs


class UserLoginSerializer(serializers.Serializer):
    """Serializer data for login users."""

    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    default_error_messages = {
        'inactive_account': _('User account is disabled.'),
        'invalid_credentials': _('Unable to login with provided credentials.')
    }

    def __init__(self, *args, **kwargs):
        """Initialize Serializer."""
        super(UserLoginSerializer, self).__init__(*args, **kwargs)
        # set None for reason: it useless for us here
        self.user = None

    def validate(self, attrs):
        """Validate user's login and password."""
        self.user = authenticate(username=attrs.get("username"),
                                 password=attrs.get('password'))
        if self.user:
            if not self.user.is_active:
                raise serializers.ValidationError(
                    self.error_messages['inactive_account'])
            return attrs
        else:
            raise serializers.ValidationError(
                self.error_messages['invalid_credentials'])


class TokenSerializer(serializers.ModelSerializer):
    """Serializer data for authorization token."""

    auth_token = serializers.CharField(source='key')

    class Meta:
        """Meta-data for auth-token."""

        model = Token
        fields = ("auth_token", "created")
