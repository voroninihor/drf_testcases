"""Test-cases for user API."""
import json

from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase


class BaseTestCase(APITestCase):
    """Base class for tests."""

    def setUp(self):
        """Set up environment."""
        self.user_data = {
            "username": "test_user",
            "email": "testmail@mail.com",
            "password": "password",
            "confirm_password": "password"
        }


class UserRegistrationAPIViewTestCase(BaseTestCase):
    """Test-cases for user's registration API."""

    url = reverse("users:list")

    def test_invalid_password(self):
        """Test creation user with invalid password confirmation."""
        self.user_data["confirm_password"] = ["INVALID_PASSWORD"]
        response = self.client.post(self.url, self.user_data)
        self.assertEqual(400, response.status_code)

    def test_user_registration(self):
        """Test registration of user with valid data."""
        response = self.client.post(self.url, self.user_data)
        self.assertEqual(201, response.status_code)
        self.assertTrue("token" in json.loads(response.content))

    def test_unique_username_required(self):
        """Test registration with valid data and unique name requirements."""
        response = self.client.post(self.url, self.user_data)
        self.assertEqual(201, response.status_code)

        user_data_another = self.user_data
        user_data_another["password"] = "password2"
        user_data_another["confirm_password"] = "password2"
        response = self.client.post(self.url, user_data_another)
        self.assertEqual(400, response.status_code)


class UserLoginAPIViewTestCase(BaseTestCase):
    """Test-cases for user's authorization API."""

    url = reverse("users:login")

    def setUp(self):
        """Set up environment."""
        BaseTestCase.setUp(self)
        self.user = User.objects.create_user(self.user_data["username"],
                                             self.user_data["email"],
                                             self.user_data["password"])

    def test_authentication_without_password(self):
        """Test user's authorization without password."""
        response = self.client.post(self.url, {"username": "test_user"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_wrong_password(self):
        """Test user's authorization with wrong password."""
        response = self.client.post(self.url,
                                    {"username": self.user_data["username"],
                                     "password": "wrong_password"})
        self.assertEqual(400, response.status_code)

    def test_authentication_with_valid_data(self):
        """Test user's authorization with valid password."""
        response = self.client.post(self.url,
                                    {"username": self.user_data["username"],
                                     "password": self.user_data["password"]})
        self.assertEqual(200, response.status_code)
        self.assertTrue("auth_token" in json.loads(response.content))


class UserTokenAPIViewTestCase(BaseTestCase):
    """Test-cases for user's token API."""

    def url(self, key):
        """Get URL by key."""
        return reverse("users:token", kwargs={"key": key})

    def setUp(self):
        """Set up environment."""
        BaseTestCase.setUp(self)
        self.user = User.objects.create_user(self.user_data["username"],
                                             self.user_data["email"],
                                             self.user_data["password"])
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

        self.user_2 = User.objects.create_user(
            "{}2".format(self.user_data["username"]),
            "2{}".format(self.user_data["email"]),
            "{}2".format(self.user_data["password"]))
        self.token_2 = Token.objects.create(user=self.user_2)

    def tearDown(self):
        """Tear Down environment."""
        self.user.delete()
        self.token.delete()
        self.user_2.delete()
        self.token_2.delete()

    def api_authentication(self):
        """Authorize client by token."""
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_delete_by_key(self):
        """Test delete by token."""
        response = self.client.delete(self.url(self.token.key))
        self.assertEqual(204, response.status_code)
        self.assertFalse(Token.objects.filter(key=self.token.key).exists())

    def test_delete_current(self):
        """Test delete current by token."""
        response = self.client.delete(self.url('current'))
        self.assertEqual(204, response.status_code)
        self.assertFalse(Token.objects.filter(key=self.token.key).exists())

    def test_delete_unauthorized(self):
        """Test delete unauthorized by token."""
        response = self.client.delete(self.url(self.token_2.key))
        self.assertEqual(404, response.status_code)
        self.assertTrue(Token.objects.filter(key=self.token_2.key).exists())

    def test_get(self):
        """Test get by token.

        Unauthorized access must return 404
        """
        response = self.client.get(self.url(self.token_2.key))
        self.assertEqual(404, response.status_code)

        for key in [self.token.key, 'current']:
            response = self.client.get(self.url(key))
            self.assertEqual(200, response.status_code)
            self.assertEqual(self.token.key, response.data['auth_token'])
            self.assertIn('created', response.data)
