"""URL's for service."""
from django.urls import include, path
from django.contrib import admin


# API paths
api_urls = [
    path('notes/', include('notes.urls')),
    path('', include('users.urls')),
]

# Base patterns
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(api_urls)),
]
