"""Remissions functions for app."""
from rest_framework.permissions import BasePermission


class UserIsOwnerNote(BasePermission):
    """Class for verifying owner for Note."""

    def has_object_permission(self, request, view, note):
        """Verify note's permission."""
        return request.user.id == note.user.id
