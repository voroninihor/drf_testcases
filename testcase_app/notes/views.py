"""Views for notes."""
from rest_framework.generics import ListCreateAPIView
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated

from notes.models import Note
from notes.permissions import UserIsOwnerNote
from notes.serializers import NoteSerializer


class NoteListCreateAPIView(ListCreateAPIView):
    """View for creation notes."""

    serializer_class = NoteSerializer

    def get_queryset(self):
        """Get all notes by user."""
        return Note.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        """Mixin saving a new object instance."""
        serializer.save(user=self.request.user)


class NoteDetailAPIView(RetrieveUpdateDestroyAPIView):
    """Read-write-delete endpoints to represent a single model instance."""

    serializer_class = NoteSerializer
    queryset = Note.objects.all()
    permission_classes = (IsAuthenticated, UserIsOwnerNote)
