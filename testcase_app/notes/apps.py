"""Basic config for Note app."""
from __future__ import unicode_literals

from django.apps import AppConfig


class NotesConfig(AppConfig):
    """Configuration parameters for Note app."""

    name = 'notes'
    verbose_name = 'TODOs sample application'
