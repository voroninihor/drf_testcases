"""URL's patterns for notes."""
from django.urls import path

from notes.views import NoteDetailAPIView
from notes.views import NoteListCreateAPIView


app_name = 'notes'

urlpatterns = [
    path('', NoteListCreateAPIView.as_view(), name="list"),
    path('<int:pk>/', NoteDetailAPIView.as_view(), name="detail"),
]
