"""Basic config for admin-view."""
from django.contrib import admin
from notes.models import Note


class NoteAdmin(admin.ModelAdmin):
    """Config admin-view for Note data-model."""

    list_display = ("user", "name", "done", "date_created")
    list_filter = ("done", "date_created")


admin.site.register(Note, NoteAdmin)
