"""Serialize for data-model."""
from django.contrib.auth.models import User
from rest_framework import serializers

from notes.models import Note


class NoteUserSerializer(serializers.ModelSerializer):
    """Serialization for users."""

    class Meta:
        """Meta-data for users."""

        model = User
        fields = ("id", "username", "email", "date_joined")


class NoteSerializer(serializers.ModelSerializer):
    """Serialization for notes."""

    user = NoteUserSerializer(read_only=True)

    class Meta:
        """Meta-data for notes."""

        model = Note
        fields = ("user", "name", "done", "date_created")
