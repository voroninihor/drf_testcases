"""Test-cases for API."""
import json
from django.contrib.auth.models import User
from django.urls import reverse

from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from notes.models import Note
from notes.serializers import NoteSerializer


class BaseTestCase(APITestCase):
    """Base class for tests."""

    def setUp(self):
        """Set up environment."""
        self.username = "test_user"
        self.email = "test_user_mail@test_mail.com"
        self.password = "test_pass"
        self.user = User.objects.create_user(self.username,
                                             self.email,
                                             self.password)
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        """Authenticate method."""
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)


class NoteListCreateAPIViewTestCase(BaseTestCase):
    """Test-cases for user's actions API."""

    url = reverse("notes:list")

    def test_create_note(self):
        """Test creation note by user."""
        response = self.client.post(self.url, {"name": "Add another method."})
        self.assertEqual(201, response.status_code)

    def test_user_notes(self):
        """Test to verify user's notes list."""
        Note.objects.create(user=self.user, name="Test_task")
        response = self.client.get(self.url)
        self.assertTrue(
            len(json.loads(response.content)) == Note.objects.count())


class NoteDetailAPIViewTestCase(BaseTestCase):
    """Test-cases for notes API."""

    def setUp(self):
        """Set up environment."""
        BaseTestCase.setUp(self)
        self.note = Note.objects.create(user=self.user, name="Test task")
        self.url = reverse("notes:detail", kwargs={"pk": self.note.pk})

    def test_note_object_bundle(self):
        """Test to verify note object bundle."""
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)

        note_serializer_data = NoteSerializer(instance=self.note).data
        response_data = json.loads(response.content)
        self.assertEqual(note_serializer_data, response_data)

    def test_note_object_update_authorization_rquired(self):
        """Test authorization required for change another note."""
        another_user = User.objects.create_user(
            "another_user",
            "another_user_mail@test_mail.com",
            "another_pass")
        new_token = Token.objects.create(user=another_user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + new_token.key)

        # HTTP PUT
        response = self.client.put(self.url, {"name",
                                              "Changed by another user"})
        self.assertEqual(403, response.status_code)

        # HTTP PATCH
        response = self.client.patch(self.url, {"name",
                                                "Changed by another user"})
        self.assertEqual(403, response.status_code)

    def test_note_object_update(self):
        """Test update data for note."""
        response = self.client.put(self.url, {"name": "Updated!"})
        response_data = json.loads(response.content)
        note = Note.objects.get(id=self.note.id)
        self.assertEqual(response_data.get("name"), note.name)

    def test_note_object_partial_update(self):
        """Test partial update data for note."""
        response = self.client.patch(self.url, {"done": True})
        response_data = json.loads(response.content)
        note = Note.objects.get(id=self.note.id)
        self.assertEqual(response_data.get("done"), note.done)

    def test_note_object_delete_authorization_required(self):
        """Test authorization required for delete another note."""
        new_user = User.objects.create_user("another_user",
                                            "another_user_mail@test_mail.com",
                                            "another_pass")
        new_token = Token.objects.create(user=new_user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + new_token.key)
        response = self.client.delete(self.url)
        self.assertEqual(403, response.status_code)

    def test_note_object_delete(self):
        """Test delete note by user."""
        response = self.client.delete(self.url)
        self.assertEqual(204, response.status_code)
